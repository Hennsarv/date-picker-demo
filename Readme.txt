Datepickeri lisamiseks - mis ma tegin

1. Employee mudelis lisasin HireDate ja BirthDate v�ljadele
	data annotatsioonid (nurksulgudes osad seal ees)

2. Employee Edit view peale panin nendele kahele v�ljale lisaks datepicker klassi

3. _Layout vaatele lisasin paar scripti
	datepickeri initsialiseerimiseks read 45-54
	eestikeelse toe llisamiseks read 55-97

4. Valisin nuget managerist jquery.ui miskine mooduli

5. Lisasin klassi

	DateTimeModelBinder // proovi, kas saab ka ilma

6. Lisasin kaks rida _Layout viewsse

	Rida 8 ja rida rida 41

7. Lisasin kuup�evav�ljade editorFor asjadele klassi datepicker
   @Html.EditorFor(model => model.BirthDate, 
   new { htmlAttributes = new { @class = "form-control datepicker" } })

	PEALE NEID ASJU HAKKAS MUL DATEPICKER ilusti t��le